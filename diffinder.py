#!/usr/bin/env python3

import sys
# from pathlib import Path
import difflib


def prompt():
    return input("Enter the paths. Hit 'y' when you are done.\n")


def write_file(filename, content):
    with open(filename, "a") as f:
        f.write(content)


def simple(paths, filename):
    first_text = open(paths[0]).readlines()
    for path in paths:
        text = open(path).readlines()
        write_file(filename, f"\n\n {paths[0]} >>> {path} \n\n")
        for line in difflib.unified_diff(first_text, text):
            write_file(filename, line)


def every_perm(paths, filename):
    temp_paths = paths.copy()
    for path in paths:
        for temp_path in temp_paths:
            text_1 = open(path).readlines()
            text_2 = open(temp_path).readlines()
            write_file(filename, f"\n\n {path} >>> {temp_path} \n\n")
            for line in difflib.unified_diff(text_1, text_2):
                write_file(filename, line)


def main(args):
    filename = input("Output filename?\n")
    perms = input("Every permutation? y/n\n")

    paths = []
    while True:
        path = prompt()
        if path == "y":
            break
        else:
            paths.append(path)

    if perms == "n":
        simple(paths, filename)
    else:
        every_perm(paths, filename)


if __name__ == '__main__':
    main(sys.argv[1:])
